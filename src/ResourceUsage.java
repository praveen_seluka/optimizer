
public class ResourceUsage {
  double cpu;
  double memory;
  double factor;

  ResourceUsage(double cpu, double memory) {
    this.cpu = cpu;
    this.memory = memory;
    factor = 0.10; // Increase, decrease resources by 10%
  }

  void setFactor(double factor) {
    this.factor = factor;
  }

  ResourceUsage reduceByFactor() {
    double newCpu = cpu - (cpu * 0.10);
    double newMemory = memory - (memory * 0.10);
    if(newCpu < ResourceBounds.MIN_CPU) {
      newCpu = ResourceBounds.MIN_CPU;
    }
    if(newMemory < ResourceBounds.MIN_MEMORY) {
      newMemory = ResourceBounds.MIN_MEMORY;
    }
    return new ResourceUsage(newCpu, newMemory);
  }

  ResourceUsage increaseByFactor() {
    double newCpu = cpu + (cpu * 0.10);
    double newMemory = memory + (memory * 0.10);
    if(newCpu > ResourceBounds.MAX_CPU) {
      newCpu = ResourceBounds.MAX_CPU;
    }
    if(newMemory > ResourceBounds.MAX_MEMORY) {
      newMemory = ResourceBounds.MAX_MEMORY;
    }
    return new ResourceUsage(newCpu, newMemory);
  }
}

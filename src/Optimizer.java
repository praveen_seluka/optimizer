
public class Optimizer {
  static IClusterManager clusterManager = new ClusterManager();

  public static void main(String args[]) throws Exception {

    ResourceUsage resources [] = new ResourceUsage[2];
    int iterationThreshold = 5;
    // Defaulting to initial resource for each service for test purpose
    // We just have two services, front end and back end
    for(int i=0;i<2;i++) {
      resources[i] = new ResourceUsage(1, 100);
      //Deploy the services with initial resource requirements
      clusterManager.deployService(i, resources[i]);
    }

    /**
     * At high level - we do this in a loop
     * 1. find critical component, the resource intensive one amongst front-end vs back-end
     * 2. On the resource intensive service (say sx), it does 2-d grid search to find the best cpu, memory allocation that satisfies p95
     * 3. Deploy sx with new optimal resource allocation
     * 4. With sx fixed, it takes the less resource intensive service (say sy), repeats grid search again
     * 5. Deploy sy with new optimal resource allocation
     *
     *
     * 0. when we find that we are able to meet p95 consistently for some x iterations, we break out of the loop and consider that as optimal
     *  Steps 2 and 3 are done so that we don't do extensive grid search on 4-d space
     */
    int numIterationsOptimalFound = 0;
    while(true) {
      if (numIterationsOptimalFound == iterationThreshold )
        break;
      //add termination condition
      int testId = clusterManager.runLoadTest(5);
      int serviceId = findCriticalComponent(testId, resources);//s1

      // Find best resource usage that satisifies p95 for critical service
      optimalResourceUsage = null; currentState = State.NOTFOUND;
      gridSearch(testId, resources[serviceId], serviceId);//s1
      resources[serviceId]  = optimalResourceUsage;
      clusterManager.deployService(serviceId, resources[serviceId]);

      // With critical component fixes, find best resource usage for the other service
      optimalResourceUsage = null; currentState = State.NOTFOUND;
      gridSearch(testId, resources[1 - serviceId], 1 - serviceId);//s2
      resources[1 - serviceId] = optimalResourceUsage;
      clusterManager.deployService(1 - serviceId, resources[1-serviceId]);

      if(currentState == State.OPTIMALFOUND) {
        numIterationsOptimalFound++;
      }

    }
    System.out.println("Deployed services with optimal resource allocation");
  }
  static ResourceUsage optimalResourceUsage = null;
  static State currentState = State.NOTFOUND;

  static void gridSearch(int testId, ResourceUsage resourceUsage, int serviceId) {

    if (currentState == State.OPTIMALFOUND || currentState == State.BOUNDARYFOUND)
      return;

    Percentiles p = clusterManager.getLatencyFromTest(testId);
    ResourceUsage newResourceUsage;
    // If p95 met, keep reducing resources till p95 not met
    if(p.p95 <= 750) {
      optimalResourceUsage = resourceUsage;
      currentState = State.P95FOUND;
      //try reducing it
      newResourceUsage = resourceUsage.reduceByFactor();
    }
    else {
      // We reduced the resources and now p95 is not met, last successful p95 resource is optimal
      if (currentState == State.P95FOUND) {
        currentState = State.OPTIMALFOUND;
      }
      //increase resource Usage
      newResourceUsage = resourceUsage.increaseByFactor();
    }

    // Some boundary checking each time we increase/decrease the resource
    if (resourceUsage.cpu == ResourceBounds.MAX_CPU && resourceUsage.memory == ResourceBounds.MAX_MEMORY) {
      // we have increased the resource allocation to maximum possible. Cant do better than this
      currentState = State.BOUNDARYFOUND;
      optimalResourceUsage = resourceUsage;
      return;
    }

    if (resourceUsage.cpu == ResourceBounds.MIN_CPU && resourceUsage.memory == ResourceBounds.MIN_MEMORY) {
      // We have reduced the resource allocation to minimum possible. We reduce it only when p95 is met, hence we reached the minimum with p95 met. Hence this is optimal
      currentState = State.BOUNDARYFOUND;
      optimalResourceUsage = resourceUsage;
      return;
    }

    clusterManager.deployService(serviceId, newResourceUsage);
    testId = clusterManager.runLoadTest(5);
    gridSearch(testId, newResourceUsage, serviceId);
  }

  enum State {
    NOTFOUND,P95FOUND,OPTIMALFOUND,BOUNDARYFOUND;
  }

  /**
   * Finds the critical component amongst the front-end or back-end service
   * It uses a simple algorithm as of now :It just finds the max utilization of c1, c2, m1, m2 (where these are
   * utilizations observed in actual run) and defines that component as critical
   */
  static int findCriticalComponent(int testId, ResourceUsage initConfig[]) {

    double cpuUtilization[] = new double[2];
    double memoryUtilization[] = new double[2];
    for(int i=0;i<2;i++) {
      ResourceUsage rs[] = clusterManager.getResourceUsageFromTest(testId, i);

      double avgCpu, avgMemory;
      double sumCpu = 0.0;
      double sumMemory = 0.0;
      for (ResourceUsage t : rs) {
        sumCpu += t.cpu;
        sumMemory += t.memory;
      }
      avgCpu = sumCpu / rs.length;
      avgMemory = sumMemory / rs.length;

      cpuUtilization[i] = avgCpu;//TODO
      memoryUtilization[i] = avgMemory;
    }


    int criticalResource = -1;
    double maxUtilization = 0.0;
    for(int i=0;i<2;i++) {
      double maxUtilizationForResource = Math.max(cpuUtilization[i], memoryUtilization[i]);
      if (maxUtilizationForResource > maxUtilization) {
        maxUtilization = maxUtilizationForResource;
        criticalResource = i;
      }
    }
    return criticalResource;
  }
}


public interface IClusterManager {
    void deployService(int serviceId, ResourceUsage resourceUsage);
    int runLoadTest(int minutes);
    Percentiles getLatencyFromTest(int testId);
    ResourceUsage[] getResourceUsageFromTest(int testId, int serviceId);
}


public class ResourceBounds {
  static double MIN_CPU = 1;
  static double MAX_CPU = 8;
  static double MIN_MEMORY = 1;
  static double MAX_MEMORY = 20;
}
